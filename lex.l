%{
/* alck - check sendmail-style alias files for consistency
   Copyright (C) 2005, 2007, 2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */
	
#include "alck.h"
#include "y.tab.h"	
#include <ctype.h>

static void line_begin (void);
static void line_add (char *text, size_t len);
static void line_add_unescape (char *text, size_t len);
static void line_finish (void);

static SLIST *slist;
 
char *file_name;
int line_num;
 
%}

%option noinput
%option nounput

%x STR
IDENT [a-zA-Z0-9_\-+\./]+
WS [ \t]
SPEC [:@\\]
%%
  /* Comments */
#.*\n   { line_num++; return EOL; }
  /* White space */
^{WS}+\n { line_num++; return EOL; }
{WS}+    ;
  /* Names and emails */
:include:  return INCLUDE;
^{IDENT}  {
	  line_begin();
          line_add(yytext, yyleng);
          line_finish ();
	  return LHS; }
{IDENT}@{IDENT} {
	  line_begin();
          line_add(yytext, yyleng);
          line_finish();
	  return EMAIL; }
{IDENT} { line_begin();
          line_add(yytext, yyleng);
          line_finish();
	  return IDENT; }
  /* Quoted strings */
\"[^\\"\n]*\"   { line_begin();
                  line_add(yytext, yyleng);
                  line_finish();
                  return STRING; }
\"[^\\"\n]*\\.    { BEGIN(STR);
                   line_begin();
		   line_add_unescape(yytext + 1, yyleng - 1); }
<STR>[^\\"\n]*\\. { line_add_unescape(yytext, yyleng); }
<STR>[^\\"\n]*\" { BEGIN(INITIAL);
                   if (yyleng > 1) 
                     line_add(yytext, yyleng - 1); 
                   line_finish();
		   return STRING; }
  /* Other characters */
{SPEC}  return yytext[0];
\\\n    { line_num++; }
\n{WS}+/[^ \t\n]  { line_num++; return CONT; }
\n      { line_num++; return EOL; }
,	return yytext[0];
.       { error("%s:%d: stray character %03o in alias file", file_name,
                line_num, yytext[0]);
          error_count++; }
%%

int
yywrap()
{
	fclose(yyin);
	free(file_name);
	file_name = NULL;
	return 1;
}

static char escape_transtab[] = "\\\\a\ab\bf\fn\nr\rt\t";

int
unescape_char(int c)
{
	char *p;
	
	for (p = escape_transtab; *p; p += 2) {
		if (*p == c)
			return p[1];
	}
	return c;
}

void
line_add(char *text, size_t len)
{
	slist_add_n(&slist, text, len);
}

void
line_add_unescape(char *text, size_t len)
{
	char *p = slist_add_n(&slist, text, len);
	p[len - 1] = unescape_char(text[len - 1]);
}

void
line_begin()
{
	slist_destroy(&slist);
}

void
line_finish()
{
	size_t len = 0;
	struct string_list *sp;
	char *p;
	
	for (sp = slist->head; sp; sp = sp->next)
		len += sp->len;
	yylval.string = emalloc(len + 1);
	p = yylval.string;
	for (sp = slist->head; sp; sp = sp->next) {
		memcpy(p, sp->str, sp->len);
		p += sp->len;
	}
	*p = 0;
}

void
openaliases(char *name)
{
	yyin = fopen(name, "r");
	if (!yyin) {
		syserror(errno, "cannot open file `%s'", name);
		exit(1);
	}
	file_name = strdup(name);
	line_num = 0;
}

void
openaliases_prefix(char *prefix, char *name)
{
	char *fullname = NULL;
	struct stat st;
	
	if (stat(prefix, &st)) {
		syserror(errno, "cannot stat `%s'", prefix);
		exit(1);
	}

	if (!S_ISDIR(st.st_mode)) {
		char *p = strrchr(prefix, '/');
		if (p)
			*p = 0;
		else
			prefix = ".";
	}
	fullname = emalloc(strlen(prefix) + strlen(name) + 2);
	strcpy(fullname, prefix);
	strcat(fullname, "/");
	strcat(fullname, name);
	openaliases(fullname);
	free(fullname);
}

void
init_lex()
{
	yy_flex_debug = 0;
}

void
lex_debug(int debug)
{
	yy_flex_debug = debug;
}

void
freadlist(SLIST **plist, char *name)
{
	char *p;
	char buffer[256];
	FILE *fp;
	int line = 0;
	int skipeol = 0;

	fp = fopen(name, "r");
	if (!fp) {
		parserror(file_name, line_num,
			  "cannot open include file `%s': %s",
			  name, strerror(errno));
		error_count++;
		return;
	}

	while ((p = fgets(buffer, sizeof buffer, fp)) != NULL) {
		size_t len = strlen(p);

		line++;
		
		if (len == 0)
			continue;
		if (p[len-1] != '\n') {
			if (!feof(fp)) {
				if (!skipeol)
					parserror(name, line,
						  "line too long");
				error_count++;
				skipeol = 1;
			}
		} else if (skipeol)
			continue;
		else {
			p[--len] = 0;
			skipeol = 0;
		}

		while (*p && isspace(*p))
			p++;
		if (!*p || *p == '#')
			continue;

		slist_add(plist, p);
	}
	fclose(fp);
}

void
preadlist(SLIST **plist, char *progname)
{
	char *p;
	char buffer[256];
	FILE *fp;
	int line = 0;
	int skipeol = 0;

	fp = popen(progname, "r");
	if (!fp) {
		parserror(file_name, line_num,
			  "cannot run `%s': %s",
			  progname, strerror(errno));
		error_count++;
		return;
	}
	
	while ((p = fgets(buffer, sizeof buffer, fp)) != NULL) {
		size_t len = strlen(p);

		line++;
		
		if (len == 0)
			continue;
		if (p[len-1] != '\n') {
			if (!feof(fp)) {
				if (!skipeol)
					parserror(progname, line,
						  "line too long");
				error_count++;
				skipeol = 1;
			}
		} else if (skipeol)
			continue;
		else {
			p[--len] = 0;
			skipeol = 0;
		}

		if (!*p || !isalnum(*p) || p[strcspn(p, " \t")])
			continue;
		
		slist_add(plist, p);
	}
	pclose(fp);
}	
