CFLAGS=-O2 -g -Wall
PREFIX=/usr/local
BINDIR=$(PREFIX)/bin
MANDIR=$(PREFIX)/share/man
PACKAGE=alck
VERSION=1.0
DISTFILES=COPYING README Makefile gram.y lex.l $(SRCS) $(INCS) alck.1
SRCS=lex.yy.c y.tab.c alck.c slist.c
OBJS=$(SRCS:.c=.o)
INCS=alck.h y.tab.h

alck: $(OBJS)
	cc $(CFLAGS) -o alck $(OBJS)

y.tab.c y.tab.h: gram.y
	yacc -vtd gram.y

lex.yy.c: lex.l
	lex -d lex.l

lex.yy.o: lex.yy.c y.tab.h

clean:;	rm -f *.o alck

allclean: clean
	rm -f lex.yy.c y.tab.[ch]

install-bin: alck
	mkdir -p $(DESTDIR)$(BINDIR)
	cp alck $(DESTDIR)$(BINDIR)

install-man: alck.1
	mkdir -p $(DESTDIR)$(MANDIR)/man1
	cp alck.1 $(DESTDIR)$(MANDIR)/man1

install: install-bin install-man

distdir = $(PACKAGE)-$(VERSION)

distdir: $(DISTFILES)
	rm -rf $(distdir)
	mkdir $(distdir)
	cp $(DISTFILES) $(distdir)

dist: distdir
	tar cfz $(distdir).tar.gz $(distdir)
	rm -rf $(distdir)

distcheck: distdir
	mkdir $(distdir)/_inst; \
	cd $(distdir) || exit 2;\
	make || exit 2; \
	make DESTDIR=`pwd`/_inst install || exit 2
	make dist
