/* alck - check sendmail-style alias files for consistency
   Copyright (C) 2005, 2007, 2013, 2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#include "alck.h"

char *
slist_add_n(SLIST **plist, char *str, size_t n)
{
	struct string_list *p = emalloc(sizeof(*p));
	p->str = emalloc(n);
	memcpy(p->str, str, n);
	p->len = n;
	p->next = NULL;

	if (!*plist) {
		*plist = emalloc(sizeof(**plist));
		(*plist)->head = NULL;
	}

	if ((*plist)->head == NULL) {
		(*plist)->head = p;
		(*plist)->count = 0;
	} else {
		(*plist)->tail->next = p;
	}
	(*plist)->count++;
	(*plist)->tail = p;
	return p->str;
}

char *
slist_add(SLIST **plist, char *str)
{
	return slist_add_n(plist, str, strlen(str) + 1);
}

void
slist_append(SLIST **pdst, SLIST *src)
{
	if (!*pdst) {
		*pdst = emalloc(sizeof(**pdst));
		(*pdst)->head = (*pdst)->tail = NULL;
		(*pdst)->count = 0;
	}

	if ((*pdst)->head == NULL)
		(*pdst)->head = src->head;
	else
		(*pdst)->tail->next = src->head;
	
	(*pdst)->tail = src->tail;
	(*pdst)->count += src->count;
}

char *
slist_member(SLIST *plist, char *name)
{
	struct string_list *p;
	
	if (plist)
		for (p = plist->head; p; p = p->next)
			if (p->str && strcmp(p->str, name) == 0)
				return p->str;
	return NULL;
}

void
slist_destroy(SLIST **plist)
{
	struct string_list *p;
	if (!plist || !*plist)
		return;
	p = (*plist)->head;
	while (p) {
		struct string_list *next = p->next;
		free(p);
		p = next;
	}
	free(*plist);
	*plist = NULL;
}
