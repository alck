/* alck - check sendmail-style alias files for consistency
   Copyright (C) 2005, 2024 Sergey Poznyakoff

   This program is free software; you can redistribute it and/or modify it
   under the terms of the GNU General Public License as published by the
   Free Software Foundation; either version 3 of the License, or (at your
   option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License along
   with this program. If not, see <http://www.gnu.org/licenses/>. */

#include <stdio.h>
#include <stdlib.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>	
#include <unistd.h>  
#include <sys/file.h>
#include <sys/stat.h>
#include <errno.h>
#include <string.h>

extern char *file_name;
extern int line_num;
extern int error_count;

void *emalloc(size_t size);
void error(const char *fmt, ...);
void syserror(int ec, const char *fmt, ...);
void parserror(const char *file, int line, const char *fmt, ...);

void init_lex();
void lex_debug(int n);
void openaliases(char *name);
void openaliases_prefix(char *prefix, char *name);

struct string_list {
	struct string_list *next;
	char *str;
	size_t len;
};

typedef struct slist {
	struct string_list *head, *tail;
	int count;
} SLIST;

char *slist_add(SLIST **plist, char *str);
char *slist_add_n(SLIST **plist, char *str, size_t n);
void slist_append(SLIST **pdst, SLIST * src);
char *slist_member(SLIST *plist, char *name);
void slist_destroy(SLIST **plist);

void freadlist(SLIST **plist, char *name);
void preadlist(SLIST **plist, char *progname);

void regalias(char *name, SLIST *exp);
void end_aliases(void);
void check_aliases(void);

extern SLIST *cw_list;
extern int verbose;

int yylex(void);
void yyerror(char const *msg);
